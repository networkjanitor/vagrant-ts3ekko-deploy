vagrant-ts3ekko-deploy
======================


requirements
------------

vagrant
'''''''

This machine was only tested on the libvirt hypervisor (=> no vmware, linux only). The tested configured requires the installation of the vagrant-libvirt plugin as well as an installation of libvirt on the host machine. The vagrant-libvirt plugin can be installed via ``vagrant plugin install vagrant-libvirt``.

ansible
'''''''

This machine is provisioned by an ansible playbook. Since the playbook includes usages of docker modules, it requires a theoretical version of ansible>=2.1. This configuration was however only tested with ansible==2.4.2.0.

installation
------------

Put this repository somewhere on a host machine which has internet access, as it'll pull the required docker images (database, manager and client) from docker hub (from https://hub.docker.com/r/katakowl/ts3ekkoclient/ and https://hub.docker.com/r/katakowl/ts3ekkomanage/). **After** configuring the ansible playbook, do a ``vagrant up`` and you should be good to go.

Sometimes, for whatever reasons, the provisioning has hickups (docker repo not properly added, etc). In this case a ``vagrant provision`` or ``vagrant reload --provision`` will usually solve the problem.

configuration
-------------

variables
'''''''''

There are two different kinds of variables. One of them are the variables used for the ansible playbook execution. These define things like the to-be-used database credentials and options regarding the docker/docker-compose initialization. The other kind are the variables used for the configuration of the actual ts3ekko software.

The scope of the ts3ekko variables is defined by the ts3ekkoutil module: https://gitlab.com/networkjanitor/ts3ekkoutil. In the ansible vars file these are all bundled in the ``ekkomanage_dc_envvar`` dictionary. Defaults for this are defined by the ts3ekkoutil module.

remember to adjust these defaults on deploy

.. code-block::

  defaults:

      POSTGRES_USER: ekko
      POSTGRES_PASSWORD: ekkopassword
      POSTGRES_DB: ekkodb

      DOCKER_PREPULL_TS3EKKOMANAGE_NAME: katakowl/ts3ekkomanage
      DOCKER_PREPULL_TS3EKKOMANAGE_TAG: latest
      DOCKER_PREPULL_TS3EKKOCLIENT_NAME: katakowl/ts3ekkoclient
      DOCKER_PREPULL_TS3EKKOCLIENT_TAG: latest
      DOCKER_COMPOSE_ROOT_DIR: /root/

      HOST_SEED_DIRECTORY: './seed/'

  vars:

      ekkomanage_dc_envvar:
        EKKO_TS3_SERVER_HOST: 'xyooz.net'
        EKKO_TS3_USERNAME: 'Ekko Bot'
        ... (see ts3ekkoutil python3 module)


seed
''''

=> :doc:`docs/seed`

local media
'''''''''''

Put the media files you want to make available to the bot into ``./media/``, that directoy will be on provisioning copied into the vargant machine and will be subsequently mounted into the client containers of the application. A test file is provided in there (from Aaron Dunn / https://musopen.org/music/19797-menuets-in-g-major-bwv-anh114-115/).

related projects
----------------

- https://gitlab.com/networkjanitor/ts3ekkoclient
- https://gitlab.com/networkjanitor/ts3ekkoutil
- https://gitlab.com/networkjanitor/ts3ekkomanage