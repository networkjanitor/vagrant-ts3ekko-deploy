Seed
====

The seed process will only look at the seed data on the very first provisioning. That means if you wrongly configure any seed data, you are left with two choices: delete the docker database container/volume in the machine (e.g. by erasing the machine alltogether or going into the machine with ``vagrant ssh`` and afterwards stopping and purging the docker environment) or you'll have to add the data via the manager api/database edits.

Seeds include the following data:

- identities.yaml => TeamSpeak3 identities used for the clients
- permission.yaml => permission configuration (which entities are allowed which ekko permissions)
- permission_doc.yaml => information regarding the existing ekko permissions (descriptions, usages)

Example files and defaults for seeds are included in the seed directory of the networkjanitor.ts3ekko role.

Sequences are initialised at 1001 with an increment of 1. If your seed data should not be re-inserted every tine the ts3ekkomanage gets restarted, then please configure them correctly.

The safest way for that is to define all your seed data with ids below 1000. This way they raise an expected IntegrityError on re-start and won't pollute your database. On the tables that include a UniqueConstraint on a column (which is not a Sequence) you can also ignore this.

For seed initialisation the following library is used: https://github.com/heavenshell/py-sqlalchemy_seed

ts3 identity seed
`````````````````

You need to pay special attention to identities that you want to seed the ts3ekko bot with. In the replacement process (provisioning the ts3client database) it sometimes occours (based on the select ts3 identity) that the ts3client database (settings.db) gets corrupted. The only indication for that is a restarting ts3ekkoclient container and the log messages that the application could not connect to the TS3ClientQuery endpoint.

To test your identities (before seeding your prod system) you want to set up a test system, seed all your identities and try to spin up all ekko client instances until you are out of identites. If one fails to spin up, it is most likely because of a broken identity. If the test replacement process worked for the given identities, then it safe to use them.

You can check for that by accessing the logs of the specific container.

#. ``vagrant ssh``
#. ``sudo su``
#. ``docker logs -f <container name>``

permission seed
```````````````

By default the packages seeds some 'sane' permissions. If this is not what you want, then I highly suggest you have a look into the ``seed/permission.yaml`` file.
For the list of available permissions check out the list at https://gitlab.com/networkjanitor/ts3ekkoclient/blob/master/docs/permission.rst